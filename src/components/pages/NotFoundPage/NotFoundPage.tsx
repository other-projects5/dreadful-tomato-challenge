
import * as React from 'react';
import { FunctionComponent } from 'react';

import styles from './NotFoundPage.module.scss';

export const NotFoundPage: FunctionComponent = () => {

	return (
		<h1 className={styles.message} >
			Not found!
		</h1>
	);
};
