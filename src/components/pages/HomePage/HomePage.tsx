
import * as React from 'react';
import { FunctionComponent } from 'react';

import { Hero } from './Hero';

export const HomePage: FunctionComponent = () => {

	return (
		<Hero />
	);
};
