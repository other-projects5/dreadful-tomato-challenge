
import * as React from 'react';
import { FunctionComponent, ReactNode } from 'react';
import { useHistory } from 'react-router-dom';

import { routes } from '../../../routes/config/routes';
import SeriesImg from '../../../../assets/images/breaking-bad.jpg';
import MoviesImg from '../../../../assets/images/avengers.jpg';

import styles from './Hero.module.scss';

export const Hero: FunctionComponent = () => {

	const history = useHistory();

	const onSectionClick = (url: string): void => {
		history.push(url);
	};

	const renderSection = (image: string, text: string): ReactNode => {

		const url = text === 'Series' ? routes.series : routes.movies;

		return (
			<div className={styles.section} onClick={() => onSectionClick(url)} >
				<img className={styles.sectionImage} src={image} alt={text} />
				<div className={styles.sectionLabel} >
					{text}
				</div>
			</div>
		);
	};

	return (
		<div className={styles.heroWrapper} >
			{renderSection(MoviesImg, 'Movies')}
			{renderSection(SeriesImg, 'Series')}
		</div>
	);
};
