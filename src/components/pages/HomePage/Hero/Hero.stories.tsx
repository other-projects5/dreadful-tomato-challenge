
import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Hero } from './Hero';

export default {
	title: 'Pages/Home/Hero',
	component: Hero,
} as ComponentMeta<typeof Hero>;

const Template: ComponentStory<typeof Hero> = (args) => (<Hero {...args} />);

export const Primary = Template.bind({});
