
import * as React from 'react';
import { Provider } from 'react-redux';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { initialState } from '../../../store/initialState';
import { IAppState } from '../../../store/IAppState';

import { CatalogPage } from './CatalogPage';

import { createStoryStore } from '../../utils/storybook';


const state: IAppState = {
	...initialState
};

const store = createStoryStore(state);

export default {
	title: 'Pages/Catalog',
	component: CatalogPage,
	args: {
		type: 'movie'
	},
	decorators: [
		(CatalogPage) => (
			<Provider store={store} >
				<CatalogPage />
			</Provider>
		)
	]
} as ComponentMeta<typeof CatalogPage>;

const Template: ComponentStory<typeof CatalogPage> = (args) => (<CatalogPage {...args} />);

export const Primary = Template.bind({
	type: 'movie'
});
