
import * as React from 'react';
import { FunctionComponent, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Container } from '../../../__lib__/react-components';

import { Dispatch } from '../../../store/types/redux';
import { fetchContentMediaByType, setSkip } from '../../../store/actions/appDataActions';

import { MediaContentType } from '../../../models';
import { GridViewContent } from '../../Partials';

import styles from './CatalogPage.module.scss';


interface CatalogPageProps {
	type: MediaContentType;
}

export const CatalogPage: FunctionComponent<CatalogPageProps> = (props) => {

	const dispatch: Dispatch = useDispatch();
	const dispatchFetchContentMediaByType = (state: MediaContentType) => dispatch(fetchContentMediaByType(state));
	const dispatchSetSkip = (state: number) => dispatch(setSkip(state));

	const { type } = props;

	useEffect(() => {

		dispatchFetchContentMediaByType(type);
		dispatchSetSkip(1);

	}, [type]);

	return (
		<div className={styles.catalogWrapper} >
			<Container style={{ display: 'flex', justifyContent: 'center' }} >
				<GridViewContent title={type === 'movie' ? 'Popular Movies' : 'Popular Series'} rowsAmount={2} />
			</Container>
		</div>
	);
};
