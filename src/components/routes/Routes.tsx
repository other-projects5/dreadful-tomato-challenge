
import * as React from 'react';
import { Fragment, FunctionComponent } from 'react';
import { Switch, Route } from 'react-router-dom';

import { NotFoundPage, HomePage, CatalogPage } from '../pages';

import { routes } from './config/routes';
import { Header, Footer } from '../Partials';


export const Routes: FunctionComponent = () => {

	return (
		<Fragment>
			<Header />
			<Switch>
				<Route exact path={routes.home} component={HomePage} />
				<Route exact path={routes.series} render={() => (<CatalogPage type="series" />)} />
				<Route exact path={routes.movies} render={() => (<CatalogPage type="movie" />)} />

				<Route path="*" component={NotFoundPage} />
			</Switch>
			<Footer />
		</Fragment>
	);
};
