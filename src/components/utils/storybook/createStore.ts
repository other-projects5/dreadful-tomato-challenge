
import { createStore, applyMiddleware, Store } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import { IAppState } from '../../../store/IAppState';
import { initialState } from '../../../store/initialState';
import { rootReducer } from '../../../store/reducers/rootReducer';


const middleware = [thunk];

export const createStoryStore = (changes: Partial<IAppState> = {}): Store => {

	return createStore(
		rootReducer,
		{
			...initialState,
			...changes
		},
		composeWithDevTools(applyMiddleware(...middleware))
	);
};
