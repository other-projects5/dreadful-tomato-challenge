
export function fromLength(length: number): number[] {
	return Array.from(Array(length).keys());
}
