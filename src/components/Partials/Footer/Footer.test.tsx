
import { configure, shallow } from 'enzyme';
// @ts-ignore
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

import { Footer } from './Footer';


configure({ adapter: new Adapter() });

// Using StoryBook this kind of test have no sense
describe('Footer', () => {

	test('Should be rendered', () => {
		// Mock
		const wrapper = shallow(<Footer />);

		// Assertion
		expect(wrapper.find('.footerWrapper').length).toBe(1);
	});
});
