
import * as React from 'react';
import { FunctionComponent } from 'react';

import classNames from 'classnames';
import { Container } from '../../../__lib__/react-components';

import LogoImg from '../../../assets/images/logo.png';
import GooglePlayImg from '../../../assets/images/google-play.png';
import AppleStoreImg from '../../../assets/images/app-store.png';

import styles from './Footer.module.scss';


export const Footer: FunctionComponent = () => {

	return (
		<div className={styles.footerWrapper} >
			<Container>
				<img className={classNames(styles.section, styles.image)} src={LogoImg} alt="Logo - Dreadful Tomato" />
				<div className={classNames(styles.section, styles.links)} >
					<div>Home</div>
					<div>Terms of Use</div>
					<div>Legal Notices</div>
					<div>Help</div>
					<div>Manage Account</div>
				</div>
				<div className={classNames(styles.section, styles.stores)} >
					<img className={styles.storeApp} src={GooglePlayImg} alt="Google Play link" />
					<img className={styles.storeApp} src={AppleStoreImg} alt="App Store link" />
				</div>
				<div className={styles.section} >
					Copyright 2020 Dreadful Tomato Streaming. All Rights Reserved.
				</div>
			</Container>
		</div>
	);
};
