
import * as React from 'react';
import { FunctionComponent, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';

import { Dispatch } from '../../../../store/types/redux';
import { IAppState } from '../../../../store/IAppState';
import { setSkip } from '../../../../store/actions/appDataActions';

import { fromLength } from '../../../utils/array';

import styles from './Steps.module.scss';


interface StepsProps {
	rowsAmount: number;
}

export const Steps: FunctionComponent<StepsProps> = (props) => {

	const { list, itemsPerRow, skip } = useSelector((state: IAppState) => state.appData.contentList);

	const dispatch: Dispatch = useDispatch();
	const setSkipDispatch = (state: number) => dispatch(setSkip(state));

	const { rowsAmount } = props;
	const [stepsAmount, setStepsAmount] = useState<number>(1);

	useEffect(() => {
		setStepsAmount(Math.ceil(list.length / (rowsAmount * itemsPerRow)));
	}, [list]);

	const handlePrevious = (): void => {

		if (skip > 1) {
			setSkipDispatch(skip - 1);
		}
	};

	const handleNext = (): void => {

		if (skip < stepsAmount) {
			setSkipDispatch(skip + 1);
		}
	};

	return (
		<div className={styles.stepsWrapper} >
			<div className={classNames({
				[styles.symbol]: true,
				[styles.disabled]: skip === 1
				})}
				 onClick={handlePrevious} >&#60;</div>

			{ fromLength(stepsAmount).map((item, index) => (
				<div key={index}
					 className={classNames({
						 [styles.step]: true,
						 [styles.active]: index + 1 === skip
					 })}
					 onClick={() => setSkipDispatch(index + 1)} >
					{item + 1}
				</div>
			))}

			<div className={classNames({
				[styles.symbol]: true,
				[styles.disabled]: skip === stepsAmount
				})}
				 onClick={handleNext} >&#62;</div>
		</div>
	);
};
