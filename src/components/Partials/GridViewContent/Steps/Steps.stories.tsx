
import * as React from 'react';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { Steps } from './Steps';
import { Provider } from 'react-redux';

import { createStoryStore } from '../../../utils/storybook';
import { IAppState } from '../../../../store/IAppState';
import { initialState } from '../../../../store/initialState';

const state: IAppState = {
	...initialState,
	appData: {
		...initialState.appData,
		contentList: {
			...initialState.appData.contentList,
			skip: 1,
			list: [
				{
					title: "Wolf Creek",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/128/61/12861-PosterArt-ec32a81986a45eac7e080112075ab466.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2016
				},
				{
					title: "Wolf Creek",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/128/61/12861-PosterArt-ec32a81986a45eac7e080112075ab466.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2016
				},
				{
					title: "Wolf Creek",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/128/61/12861-PosterArt-ec32a81986a45eac7e080112075ab466.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2016
				},
				{
					title: "Wolf Creek",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/128/61/12861-PosterArt-ec32a81986a45eac7e080112075ab466.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2016
				},
				{
					title: "Wolf Creek",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/128/61/12861-PosterArt-ec32a81986a45eac7e080112075ab466.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2016
				},
				{
					title: "No Activity",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/106/36/10636-PosterArt-9add943c5e62c2d89f6d31458d33508a.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "No Activity",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/106/36/10636-PosterArt-9add943c5e62c2d89f6d31458d33508a.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "No Activity",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/106/36/10636-PosterArt-9add943c5e62c2d89f6d31458d33508a.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "No Activity",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/106/36/10636-PosterArt-9add943c5e62c2d89f6d31458d33508a.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "No Activity",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/106/36/10636-PosterArt-9add943c5e62c2d89f6d31458d33508a.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Billions",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg",
						metadata: {
							width: 720,
							height: 1080
						}
					},
					releaseYear: 2016
				},
				{
					title: "Billions",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg",
						metadata: {
							width: 720,
							height: 1080
						}
					},
					releaseYear: 2016
				},
				{
					title: "Billions",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg",
						metadata: {
							width: 720,
							height: 1080
						}
					},
					releaseYear: 2016
				},
				{
					title: "Billions",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg",
						metadata: {
							width: 720,
							height: 1080
						}
					},
					releaseYear: 2016
				},
				{
					title: "Billions",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/117/25/11725-PosterArt-deecf8dbd786dfa2d964413b0bf83726.jpg",
						metadata: {
							width: 720,
							height: 1080
						}
					},
					releaseYear: 2016
				},
				{
					title: "Better Call Saul",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/175/7/1757-PosterArt-fc0e2a5b18d4a662717f3cbb1827871d.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Better Call Saul",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/175/7/1757-PosterArt-fc0e2a5b18d4a662717f3cbb1827871d.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Better Call Saul",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/175/7/1757-PosterArt-fc0e2a5b18d4a662717f3cbb1827871d.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Better Call Saul",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/175/7/1757-PosterArt-fc0e2a5b18d4a662717f3cbb1827871d.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Better Call Saul",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/175/7/1757-PosterArt-fc0e2a5b18d4a662717f3cbb1827871d.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Younger",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/143/24/14324-PosterArt-a0168953a3b93681dbd60c7bcc50a0af.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Younger",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/143/24/14324-PosterArt-a0168953a3b93681dbd60c7bcc50a0af.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Younger",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/143/24/14324-PosterArt-a0168953a3b93681dbd60c7bcc50a0af.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Younger",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/143/24/14324-PosterArt-a0168953a3b93681dbd60c7bcc50a0af.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
				{
					title: "Younger",
					description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
					type: "series",
					image: {
						url: "https://streamcoimg-a.akamaihd.net/000/143/24/14324-PosterArt-a0168953a3b93681dbd60c7bcc50a0af.jpg",
						metadata: {
							width: 1000,
							height: 1500
						}
					},
					releaseYear: 2015
				},
			],
		}
	}
};

const store = createStoryStore(state);

export default {
	title: 'Partials/GridViewContent/Steps',
	component: Steps,
	args: {
		rowsAmount: 2
	},
	decorators: [
		(Steps) => (
			<Provider store={store} >
				<Steps />
			</Provider>
		)]
} as ComponentMeta<typeof Steps>;

const Template: ComponentStory<typeof Steps> = (args) => (<Steps {...args} />);

export const Primary = Template.bind({});
