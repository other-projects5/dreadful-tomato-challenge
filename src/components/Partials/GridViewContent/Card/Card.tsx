
import * as React from 'react';
import { FunctionComponent } from 'react';

import styles from './Card.module.scss';


interface CardProps {
	urlImage: string;
	title: string;
	releaseYear: number;
	description: string;
}

export const Card: FunctionComponent<CardProps> = (props) => {

	const { urlImage, title, releaseYear, description } = props;

	return (
		<div className={styles.cardWrapper} >
			<img className={styles.image} src={urlImage} alt={title} />
			<div className={styles.metadata} >
				<div className={styles.title}>{title}</div>
				<div className={styles.year}>{releaseYear}</div>
				<div className={styles.description}>{description}</div>
			</div>
		</div>
	);
};
