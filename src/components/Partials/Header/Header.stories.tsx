
import * as React from 'react';
import { MemoryRouter } from 'react-router-dom';
import { Provider } from 'react-redux';
import { ComponentMeta, ComponentStory } from '@storybook/react';

import { IAppState } from '../../../store/IAppState';
import { initialState } from '../../../store/initialState';

import { routes } from '../../routes/config/routes';
import { createStoryStore } from '../../utils/storybook';
import { Header } from './Header';


const state: IAppState = {
	...initialState
};

const store = createStoryStore(state);

export default {
	title: 'Partials/Header',
	component: Header,
	decorators: [
		(Header) => (
			<Provider store={store} >
				<MemoryRouter initialEntries={[routes.home, routes.movies, routes.series]} initialIndex={1} >
					<Header />
				</MemoryRouter>
			</Provider>
		)]
} as ComponentMeta<typeof Header>;

const Template: ComponentStory<typeof Header> = (args) => (<Header {...args} />);

export const Primary = Template.bind({});
