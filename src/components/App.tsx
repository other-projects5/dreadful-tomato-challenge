
import * as React from 'react';
import { FunctionComponent } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import { store } from '../store/store';
import { Routes } from './routes';
import '../styles/main.scss';

export const App: FunctionComponent = () => {

	return (
		<Provider store={store} >
			<BrowserRouter>
				<Routes />
			</BrowserRouter>
		</Provider>
	);
};
