
import { Reducer } from 'redux';

import { initialState } from '../initialState';
import { IAppData } from '../IAppState';
import { SET_CONTENT_MEDIA, SET_FILTER_TEXT, SET_SKIP } from '../types';


export const appDataReducer: Reducer<IAppData> = (state: IAppData = initialState.appData, { type, payload }) => {

	switch (type) {
		case SET_CONTENT_MEDIA:
			return {
				...state,
				contentList: {
					...state.contentList,
					list: payload
				}
			};

		case SET_SKIP:
			return {
				...state,
				contentList: {
					...state.contentList,
					skip: payload
				}
			};

		case SET_FILTER_TEXT:
			return {
				...state,
				filter: {
					...state.filter,
					text: payload
				}
			};

		default:
			return state;
	}
};
