
import { combineReducers, Reducer } from 'redux';

import { IAppState } from '../IAppState';
import { appDataReducer } from './appDataReducer';


export const rootReducer: Reducer<IAppState> = combineReducers({
	appData: appDataReducer
});
