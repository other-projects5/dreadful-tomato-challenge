
import { Action, Dispatch } from '../types/redux';
import { SET_CONTENT_MEDIA, SET_FILTER_TEXT, SET_SKIP } from '../types';

import { MediaContent, MediaContentType } from '../../models';

const dataBase = require('../../dataBase/data-base.json');


const bdd = (): MediaContent[] => {
	const response = JSON.parse(JSON.stringify(dataBase));
	return response.entries;
};

export const fetchContentMediaByType = (type: MediaContentType): Action => {

	return (dispatch: Dispatch) => {

		const mediaByType = bdd().filter((entry) => entry.type === type);

		dispatch({
			type: SET_CONTENT_MEDIA,
			payload: mediaByType
		});
	};
};

export const setSkip = (skip: number): Action => {

	return (dispatch: Dispatch) => {

		dispatch({
			type: SET_SKIP,
			payload: skip
		});
	};
};

export const setFilterText = (type: MediaContentType, text: string): Action => {

	return (dispatch: Dispatch) => {

		const filteredMedia = bdd()
			.filter((entry) => entry.type === type && entry.title.match(new RegExp(text, 'i')));

		dispatch({
			type: SET_CONTENT_MEDIA,
			payload: filteredMedia
		});

		dispatch({
			type: SET_SKIP,
			payload: 1
		});

		dispatch({
			type: SET_FILTER_TEXT,
			payload: text
		});
	};
};
