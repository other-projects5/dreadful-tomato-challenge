
import { MediaContent } from '../models';


export interface IAppState {
	appData: IAppData;
}

export interface IAppData {
	contentList: {
		list: MediaContent[],
		skip: number,
		itemsPerRow: number
	},
	filter: {
		text: string,
		year: number
	}
}
