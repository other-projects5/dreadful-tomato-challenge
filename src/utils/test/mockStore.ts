
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import { initialState } from '../../store/initialState';


export const mockStore = configureMockStore([ thunk ]);
export const storeStateMock = {
	...initialState
};
